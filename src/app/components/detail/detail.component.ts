import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  id: number = 0;
  dataId: any = [];

  constructor(private activatedRouter:ActivatedRoute,
    private _router: Router,
    public _homeService: HomeService,
    http: HttpClient) { }

  ngOnInit(): void {

    this.activatedRouter.params.subscribe(params => {

      this._homeService.detail(params['id']).subscribe( (resp:any) => {

        this.dataId = resp.data.results[0];
        
        console.log('Detalle', resp.data.results) 
    
       }, ( (error:Error) => {
          console.error(error);
       }))

    });

  }
  
}
