import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HomeService } from 'src/app/services/home.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  data: any = [];
  dataFull: any = [];

  constructor(
    private activatedRouter: ActivatedRoute,
    public _homeService: HomeService,
    private _router :Router
  ) { }

  ngOnInit(): void {

    this.listar();

  }


  public listar():void {

    this._homeService.getAll().subscribe( (resp:any) => {

      this.data = resp.data.results;
      this.dataFull  = resp;

      console.log('Todo',  resp)
     
      console.log('Listado',  resp.data.results)
           
    }, ( (error:Error) => {
       console.error(error);
    }))
  }

  public detalle():void {

    this._homeService.getAll().subscribe( (resp:any) => {

      this.data = resp.data.results;
           
    }, ( (error:Error) => {
       console.error(error);
    }))
  }

}
