import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { throwError } from 'rxjs';
import { Observable } from 'rxjs';
import { BASE_APIKEY, BASE_ENDPOINT } from '../config/app';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  public data: any;
  public dataActivar: any
  public dataConsultar: any; 

  protected baseEndpoint = BASE_ENDPOINT;
  protected baseApiKey = BASE_APIKEY;

  constructor(private http: HttpClient) {
    
   }

   getAll(): Observable<any> {
    return this.http.get(BASE_ENDPOINT + this.baseApiKey);
  }

  detail(id: any): Observable<any> {
    return this.http.get('http://gateway.marvel.com/v1/public/characters/' + id + this.baseApiKey);
  }

}

